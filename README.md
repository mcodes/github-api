# FrontendTest

## GOALS

#### The main goal of this tech test is to create a client side web application that reproduces the screenshots below by using [GitHub API](https://developer.github.com/v3/) .

You can see a working demo of the app in the following link: http://three-garden.surge.sh/

## SCREENSHOTS

#### First Screen

The user can search a username of GitHub

![](https://drive.google.com/uc?export=view&id=1V19nQhn7Bn3E-sBNBFtFJIatSaqLUoUj)

#### Success Screen

If the searched username does exist: The searched user profile is displayed with all his repositories

![](https://drive.google.com/uc?export=view&id=1LHyqNTFVQZQ1tdidLXLrCOxo0w6JK9oa)

#### Error Screen

If the searched username does not exist: An error is shown

![](https://drive.google.com/uc?export=view&id=1uIajOBQvo4ugtoxkga8KPbY5x2PlHh0j)
