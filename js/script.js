function loadProfile() {
  event.preventDefault();
  let username = document.getElementById('username').value;
  let url = `https://api.github.com/users/${username}`
  console.log(url)
  fetch(url)
  .then(function (res) {
    return res.json()
  })
  .then(function (data) {
    data.login ?
    (renderUserInfo(data), loadRepos(username))
    : showError()
  })
  .catch(function (error) {
    console.log('There was a problem:' + error.message);
  });
}


function loadRepos(username) {
  let url = `https://api.github.com/users/${username}/repos`
  document.getElementById('username').value='';
  fetch(url)
  .then(function (res) {
    return res.json()
  })
  .then(function (data) {
    renderUserRepos(data)
      })
  .catch(function (error) {
    console.log('There was a problem:' + error.message);
  });
}

function renderUserInfo(data) {
  let html = `
  <img class='userAvatar' src=${data.avatar_url} alt='User Avatar'/>
  <div class='userTxt'>
  <h5>@${data.login}</h5>
  <h1>${data.name}</h1>
  <h5>${data.bio}</h5>
  </div>`
  //insert into html
  document.getElementById('userProfile').innerHTML = html;
  // document.getElementById('userAvatar').src = data.avatar_url;
}

function renderUserRepos(data) {
  let title = `<h4 class='repoListTitleDivider'>Repositories</h4>`
  let html = '';
  data.forEach(repo => {
    html += `
    <a href=${repo.html_url} class='repo' target='_blank'>
    <div class='repoTitle'>${repo.name}</div>
    <div class='repoInfo'><img src='./img/star.svg'/>  ${repo.stargazers_count}  <img src='./img/repo-forked.svg'/>  ${repo.forks_count}  </div>
    </a>
    `
  });
  document.getElementById('repoListTitle').innerHTML = title;
  document.getElementById('repoList').innerHTML = html;
}


function showError() {
  let username = document.getElementById('username').value;
  document.getElementById('repoListTitle').innerHTML = '';
  document.getElementById('repoList').innerHTML = '';
  document.getElementById('username').value= ''; 
  let html = `<h5 class='errorMessage'>Username ${username} does not exist</h5>`
  document.getElementById('userProfile').innerHTML = html;
}